<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/product-data', 'HomeController@product');
Route::get('/form-product', 'HomeController@formproduct');
Route::post('/product-data', 'HomeController@addproduct');
Route::get('/product-data/{id}/edit', 'HomeController@editproduct');
Route::post('/product-data/{id}', 'HomeController@updateproduct');
Route::delete('/product-data/{id}', 'HomeController@destroy');

Route::get('/category-data', 'HomeController@category');
Route::post('/category-data', 'HomeController@addcategory');
Route::get('/form-category', 'HomeController@formcategory');
Route::get('/category-data/{id}/edit', 'HomeController@editcategory');
Route::post('/category-data/{id}', 'HomeController@updatecategory');
Route::delete('/category-data/{id}', 'HomeController@destroycat');
