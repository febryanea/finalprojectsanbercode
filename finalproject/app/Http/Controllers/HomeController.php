<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('mainuser');
    }

    public function product() {
        $products = product::all();
        return  view('product', compact('products'));
    }

    public function formproduct(){
        return  view('formproduct');
    }

    public function addproduct(Request $request){

        $request->validate([
            'name' => 'required|unique:products',
            'description' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'stock' => 'required',
        ]);

        $products = new product;
        $products->name = $request["name"];
        $products->description = $request["description"];
        $products->price = $request["price"];
        $products->category_id = $request["category_id"];
        $products->stock = $request["stock"];
        $products->save(); 

        return redirect('/product-data')->with('success', 'Product Berhasil di Tambah'); 
    }

    public function editproduct($id){
        $products = product::find($id);
        return  view('editproduct', compact('products'));
    }

    public function updateproduct($id, Request $request){
        $request->validate([
            'name' => 'required|unique:products',
            'description' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'stock' => 'required',
        ]);

        $products = product::where('id', $id)->update([
            "name" => $request["name"],
            "description" => $request["description"],
            "price" => $request["price"],
            "category_id" => $request["category_id"],
            "stock" => $request["stock"], 
        ]);
        return redirect('/product-data')->with('success', 'Data Produk Berhasil di Update'); 
    }

    public function destroy($id){
        product::destroy($id);
        return  redirect('/product-data')->with('success', 'Data produk berhasil di hapus');
    }


    public function category() {
        $categories = category::all();
        return  view('category', compact('categories'));
    }

    public function formcategory(){
        return  view('formcategory');
    }

    public function addcategory(Request $request){

        $request->validate([
            'name' => 'required|unique:products',
            'description' => 'required'
        ]);

        $categories = new category;
        $categories->name = $request["name"];
        $categories->description = $request["description"];
        $categories->save(); 

        return redirect('/category-data')->with('success', 'Category Berhasil di Tambah'); 
    }

    public function editcategory($id){
        $categories = category::find($id);
        return  view('editcategory', compact('categories'));
    }

    public function updatecategory($id, Request $request){
        $request->validate([
            'name' => 'required|unique:products',
            'description' => 'required'
        ]);

        $categories = category::where('id', $id)->update([
            "name" => $request["name"],
            "description" => $request["description"]
        ]);
        return redirect('/category-data')->with('success', 'Data Category Berhasil di Update'); 
    }

    public function destroycat($id){
        category::destroy($id);
        return  redirect('/category-data')->with('success', 'Data Category berhasil di hapus');
    }
}
