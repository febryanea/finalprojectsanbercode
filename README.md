# Final Project 

## Kelompok 100

## Anggota Kelompok

- Febryan Elfanuary Azie
- Dwiki Kurnia

## Tema Project
e-Commerce

## ERD
![alt text](ERD.png "ERD Sanbercode")

## Link Video 
Link Demo Aplikasi : https://drive.google.com/file/d/15pevNfZIFld98O6pkdm_e_iJv_fw8QMA/view?usp=sharing

Link Deploy (Optional) : http://final-project-ecommerceshop.herokuapp.com/
